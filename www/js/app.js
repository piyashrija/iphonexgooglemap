// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
   
  });
})
.config(function($stateProvider,$urlRouterProvider){
  
$stateProvider

  // setup an abstract state for the tabs directive
 .state('home', {
    url: "/home",
    templateUrl: "templates/home.html",
    controller: "homeCtrl"

  })
 .state('about', {
    url: "/about",
    templateUrl: "templates/about.html",
    controller: "aboutCtrl"

  })
 $urlRouterProvider.otherwise('/home');
})
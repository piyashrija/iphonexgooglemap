
angular.module('starter.controllers', [])

.controller('homeCtrl',function($scope,$timeout){

	
})
.controller('aboutCtrl',function($scope,$timeout){
	var map,div;
	document.addEventListener("deviceready", function() {
	  div = document.getElementById("map_canvas");

	  // Initialize the map view
	  map = plugin.google.maps.Map.getMap(div);
	 
	  // Wait until the map is ready status.
	  map.addEventListener(plugin.google.maps.event.MAP_READY, onMapReady);
	}, false);

	function onMapReady() {
		map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true)
        map.setAllGesturesEnabled(true)
        map.setCompassEnabled(false);
        map.moveCamera({
            target: { lat: -33.825444, lng: 151.242111},
            zoom: 13
        });	
        var select = document.getElementById('mapType');
		console.log('select',select)
		select.addEventListener('change', function() {
	  		alert(this.value);
	  		map.setMapTypeId(plugin.google.maps.MapTypeId[this.value]);
		})
	}
})